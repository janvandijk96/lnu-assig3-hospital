<?php

session_start();
require "includes/settings.php";

//Check if the right role
if($_SESSION['role'] == 3){
  die('Not a researcher');
}

//If insert, add a comment
if(isset($_GET['insert'])){

	//prepare vars for the database query
  $idPatient = $_GET['patient']; //ID of the patient
  $dataID = $_GET['data']; //The dataset which is being commented on
  $comment = $_GET['comment']; //Comment
  $idResearch = $_SESSION['userId']; //ID of the Physician.

  $database->query("INSERT INTO DataNote VALUES (null, $idPatient, '$dataID', '$comment', $idResearch, null);"); //Insert.

	//ELSE get a comment
} elseif (isset($_GET['patient']) && isset($_GET['data'])){
	//Get this comment by using the patient id and data set id
  $idPatient = $_GET['patient'];
  $dataID = $_GET['data'];

  //Get the data and join tables
  $result = $database->query("SELECT n.userID, n.dataID, n.comment, n.timestamp, n.ownerID, u.name FROM DataNote n INNER JOIN User u ON  n.ownerID = u.userID WHERE n.userID = $idPatient AND n.dataID = '$dataID'");
  //Loop over all the commments and create a 'block' for all of them, containing all information.

	//Create the element used by the JSON request.
	echo "<div class='comments'>";
  if(mysqli_num_rows($result) >= 1){
    while($comment = $result->fetch_assoc()):
      ?>
      <h5>Comment by <?= $comment['name']; ?></h5>
      <span><?= $comment['timestamp']; ?></span>
      <p><?= $comment['comment']; ?></p>
    <? endwhile;
  } else {
    //If there is no data available with the last query, let the user know
    echo "<p>No comment is yet available for this therapy.</p>";
  }
  echo "</div>";
}
?>

