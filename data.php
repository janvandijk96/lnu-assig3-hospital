<?php
/*
 * Below is a script from SOURCE: https://gist.github.com/robflaherty/1185299
 * I used this because the LNU server does not allow cross origin requests.
 *
 *
 * Converts CSV to JSON
 * Example uses Google Spreadsheet CSV feed
 * csvToArray function I think I found on php.net
 */
header('Content-type: application/json');

if(isset($_GET['csv'])){
  // Set your CSV feed
  $feed = $_GET['csv'];
  // Arrays we'll use later
  $keys = array();
  $newArray = array();
  // Function to convert CSV into associative array
  function csvToArray($file, $delimiter) {
    if (($handle = fopen($file, 'r')) !== FALSE) {
      $i = 0;
      while (($lineArray = fgetcsv($handle, 4000, $delimiter, '"')) !== FALSE) {
        for ($j = 0; $j < count($lineArray); $j++) {
          $arr[$i][$j] = $lineArray[$j];
        }
        $i++;
      }
      fclose($handle);
    }
    return $arr;
  }
  // Do it
  $data = csvToArray($feed, ',');
  // Set number of elements (minus 1 because we shift off the first row)
  $count = count($data) - 1;

  //Use first row for names
  $labels = array_shift($data);
  foreach ($labels as $label) {
    $keys[] = $label;
  }
  // Add Ids, just in case we want them later
  $keys[] = 'id';
  for ($i = 0; $i < $count; $i++) {
    $data[$i][] = $i;
  }

  // Bring it all together
  for ($j = 0; $j < $count; $j++) {
    $d = array_combine(array_map('strtolower', $keys), $data[$j]); //edited for lowercase.
    $newArray[$j] = $d;
  }
  // Print it out as JSON
  echo json_encode($newArray);
} else if(isset($_GET['xml'])){

  //Original code, not part of mentioned above.
  $xml = simplexml_load_file($_GET['xml']);
  echo json_encode($xml);
} else if(isset($_GET['sxml'])){

  //Original code, not part of mentioned above.
  $sxml = file_get_contents($_GET['sxml']);
  $xml = simplexml_load_string($sxml);
  echo json_encode($xml);
}
?>