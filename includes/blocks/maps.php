<?php
/**
 * Created by PhpStorm.
 * User: patrickmendel
 * Date: 07/10/2017
 * Time: 17:39
 */
?>
<div class="mdl-layout__tab-panel" id="maps">
	<section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
		<div class="mdl-card mdl-cell mdl-cell--9-col-desktop mdl-cell--6-col-tablet mdl-cell--4-col-phone">
			<div class="mdl-card__supporting-text">
				<h4>Your patients on a map</h4>
				<!-- Code from https://wrightshq.com/playground/placing-multiple-markers-on-a-google-map-using-api-3/ -->
				<div id="map_wrapper" style="width: 500px; height: 400px; margin: 0 auto;" class="mapping">
					<div id="map" style="width: 500px; height: 400px; margin: 0 auto;" class="mapping"></div>
			</div>
		</div>
	</section>
</div>

<script>
  function initMap() {
    var center = {lat: 58.2985843, lng: 12.961619};
    $.getJSON("data.php?xml=http://vhost11.lnu.se:20090/final/getData.php?table=User", function(data){
      var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 4,
        center: center
      });

      var infoWindow = new google.maps.InfoWindow();

      $.each( data['userID'], function( key, val ) {
        if(val.Role_IDrole == 1){
          var marker = new google.maps.Marker({
            position: {lat: parseInt(val.Lat), lng: parseInt(val.Long)},
            map: map,
            title: val.username
          });

          google.maps.event.addListener(marker, "click", function(e) {
            var contentstring = val.username + " (" + val.email + ")";
            infoWindow.setContent(contentstring);
            infoWindow.open(map, marker);
          });
        }
      });
    });
  }

</script>
