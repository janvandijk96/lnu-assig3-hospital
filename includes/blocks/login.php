
<section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
  <div class="mdl-card mdl-cell mdl-cell--12-col">
    <div class="mdl-card__supporting-text">
      <h4>Log in required</h4>
      Welcome to the Parkinsons Hospital Web Portal! To use this service, please log in using one of the services below.
    </div>
    <div class="mdl-card__actions">
      <a href="/authenticate.php?p=Facebook" class="mdl-button">Log in with Facebook (1, physician)</a>
      <a href="/authenticate.php?p=Twitter" class="mdl-button">Log in with Twitter (2, researcher)</a>
      <a href="/authenticate.php?p=LinkedIn" class="mdl-button">Log in with LinkedIn (3, Patient)</a>
    </div>
  </div>
</section>
