<?php
/**
 * Created by PhpStorm.
 * User: janvandijk96
 * Date: 10/10/2017
 * Time: 18:20
 */

//These videos are gathered using the API provided by YouTube and saved in videos.json.
//$url = file_get_contents('videos.json');
$url = file_get_contents('https://www.googleapis.com/youtube/v3/search/?q=parkinsons&maxResults=5&part=snippet&key=' . $youtubeApiKey);
$json = json_decode($url, true);


?>
<div class="mdl-layout__tab-panel" id="videos">
  <?php foreach($json['items'] as $video) : //Loop over the videos and display their values in the right places. ?>
  <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
    <div class="mdl-card mdl-cell mdl-cell--12-col">
      <div class="mdl-card__supporting-text">
        <h4><?= $video['snippet']['title'] ?></h4>
        Published <i><?= $video['snippet']['publishedAt'] ?></i> by <i><?= $video['snippet']['channelTitle'] ?></i><br/>
        <div class='embed-container'>
          <iframe src='http://www.youtube.com/embed/<?= $video['id']['videoId'] ?>' frameborder='0' allowfullscreen></iframe>
        </div>


      </div>
      <div class="mdl-card__actions">
        <a href="https://www.youtube.com/watch?v=<?= $video['id']['videoId'] ?>" class="mdl-button">Watch on YouTube</a>
      </div>
    </div>
  </section>
  <?php endforeach; ?>
</div>
