<?php
/**
 * Created by PhpStorm.
 * User: janvandijk96
 * Date: 10/10/2017
 * Time: 20:02
 */

$ifMed = "";

if($_SESSION['role'] == 1){
  $ifMed = "WHERE User_IDmed = " . $_SESSION['userId'];
}

// Select the therapies and join all tables to get one array with everything. If the user requesting is a physican, add $ifMed, which limits the query to its own patients. If not, all patients are requested for the researchers.
$clients = $database->query("
  SELECT 
    t.therapyID, 
    u.userID, 
    u.name,
    um.name as medName,
    u.username, 
    u.picture,
    u.email, 
    l.name as therapyName, 
    l.Medicine_IDmedicine, 
    l.Dosage, 
    m.medicineID, 
    m.name as medicineName
  FROM 
    Therapy t 
  INNER JOIN 
    User u 
    ON 
      t.User_IDpatient = u.userID 
  INNER JOIN
    User um 
    ON 
      t.User_IDmed = um.userID
  INNER JOIN Therapy_List l 
    ON 
      t.TherapyList_IDtherapylist = l.therapy_listID 
  INNER JOIN Medicine m 
    ON 
      l.Medicine_IDmedicine = m.medicineID 
  $ifMed
  ORDER BY
    t.therapyId
");

//Used for the form to add patients to a physican
$patiens = $database->query("SELECT userID, name FROM User WHERE Role_IDrole = 3");
$therapyList = $database->query("SELECT l.therapy_listID, l.name, l.dosage, m.name as medicineName  FROM Therapy_List l INNER JOIN Medicine m ON l.Medicine_IDmedicine = m.medicineID");

?>

<div class="mdl-layout__tab-panel" id="patients">
  <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
    <div class="mdl-card mdl-cell mdl-cell--12-col">
      <div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">
        <h4 class="mdl-cell mdl-cell--12-col">Patients</h4>
        <? //Loop over all the therapies and create a 'block' for all of them, containing all information.
        while ($patient = $clients->fetch_assoc()) : ?>
          <div class="section__circle-container mdl-cell mdl-cell--2-col mdl-cell--1-col-phone">
            <div class="section__circle-container__circle mdl-color--primary">
              <img src="<?= $patient['picture']; ?>" class="patient-image" />
            </div>
          </div>
          <div class="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
            <h5><?= $patient['name']; ?> (<i><?= $patient['username']; ?>, User ID <?= $patient['userID']; ?></i>)</h5>
            <ul>
              <li>Therapy: <i><?= $patient['therapyName']; ?>  (Id: <?= $patient['therapyID'] ?>)</i></li>
              <li>Medicine: <i><?= $patient['medicineName']; ?> (Dosage: <?= $patient['Dosage']; ?>, ID: <?= $patient['medicineID']; ?>)</i></li>
              <li>Physician: <i><?= $patient['medName']; ?></i></li>
            </ul>
            <?php
            //Get the users tests for the current therapy (joining all tables again)
            $therapyId = $patient['therapyID'];
            $therapy = $database->query("
              SELECT 
                n.noteId, 
                n.note, 
                n.User_IDmed,
                s.type,
                s.DataURL,
                s.test_SessionID,
                t.dateTime,
                t.Therapy_IDtherapy,
                t.testID,
                u.name
              FROM 
                Note n 
              INNER JOIN 
                Test_Session s 
                ON n.Test_Session_IDtest_session = s.test_SessionID
              INNER JOIN 
                Test t 
                ON s.Test_IDtest = t.testID
              INNER JOIN
                User u
                ON n.User_IDmed = u.userID
              WHERE 
                t.Therapy_IDtherapy = $therapyId
            ");

            if($therapy->num_rows === 0){
              //If there is no data available with the last query, let the user know
              echo "No therapy data is yet available for this patient.";
            } else {
              //If there IS data available, loop over it
              while ($therapyRow = $therapy->fetch_assoc()) :
                $randomNumber = mt_rand();
              ?>
              <div class="therapy-info">
                <!-- Allow the user to expand the UL using some dirty inline jQuery. Easy & dirty way to keep it 'tidy' for now. -->
                <h6>Therapy ID: <?= $therapyRow['test_SessionID']?> <span onclick="$('#data-<?= $randomNumber?>').show(300);">(Click here to show)</span></h6>
                <ul id="data-<?= $randomNumber ?>" style="display:none;">
                  <li>Date & Time: <?= $therapyRow['dateTime']?></li>
                  <li>Data URL: <?= $therapyRow['DataURL']?></li>
                  <li>Physican / Researcher: <?= $therapyRow['name']?></li>
                  <li>Note: <?= $therapyRow['note']?></li>
                </ul>
              </div>
            <?
            endwhile;
            } //end else (dirty)?>
          </div>
        <? endwhile; ?>
      </div>
    </div>
  </section>

  <? if($_SESSION['role'] == 1) : //If the role ID is 1 (physician), allow them to add patients to themselves. ?>
  <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp" style="margin-top: 2em;">
    <div class="mdl-card mdl-cell mdl-cell--12-col">
      <div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">
        <h4 class="mdl-cell mdl-cell--12-col">Start New Therapy</h4>
        The patient below will become YOUR client.<br/>

        <form action="/new-therapy.php" method="post" style="display: block; width: 100%;">
          Patient:
          <select id="patient" name="patient">
            <? while ($patient = $patiens->fetch_assoc()) : //List all patients in the select dropdown?>
            <option value="<?= $patient['userID'] ?>"><?= $patient['name'] ?></option>
            <? endwhile; ?>
          </select><br/>
          Therapy:
          <select id="therapy" name="therapy">
            <? while ($therapy = $therapyList->fetch_assoc()) : //List all available therapies?>
              <option value="<?= $therapy['therapy_listID'] ?>"><?= $therapy['name'] ?> (<?= $therapy['medicineName'] ?>, <?= $therapy['dosage'] ?>)</option>
            <? endwhile; ?>
          </select><br/>
          <input type="submit" value="Submit">
        </form>

      </div>
    </div>
  </section>
  <? endif; ?>
</div>
