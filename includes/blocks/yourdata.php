
<div class="mdl-layout__tab-panel" id="yourdata">
  <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
    <div class="mdl-card mdl-cell mdl-cell--12-col">
      <div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">
        <h4 class="mdl-cell mdl-cell--12-col">Your Data</h4>
				<select class="patient-selector">
					<option value="0">Choose..</option>
				</select>
				<select class="therapy-selector">

				</select>
				<p><sub>Ofcourse, this option would not be visible in a live-version. Because logged in users are not assigned a correct user ID, please choose one.</p>
				<canvas id="canvas">
					Choose a patient to continue.
				</canvas>
      </div>
    </div>
  </section>

	<section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
		<div class="mdl-card mdl-cell mdl-cell--12-col">
			<div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">
				<h4 class="mdl-cell mdl-cell--12-col">Your Therapies and data</h4>

        <?
        // Select the therapies and join all tables to get one array with everything. If the user requesting is a physican, add $ifMed, which limits the query to its own patients. If not, all patients are requested for the researchers.
        $currentUserID = $_SESSION['userId'];
				$clients = $database->query("
					SELECT 
						t.therapyID, 
						u.userID, 
						u.name,
						um.name as medName,
						u.username, 
						u.picture,
						u.email, 
						l.name as therapyName, 
						l.Medicine_IDmedicine, 
						l.Dosage, 
						m.medicineID, 
						m.name as medicineName
					FROM 
						Therapy t 
					INNER JOIN 
						User u 
						ON 
							t.User_IDpatient = u.userID 
					INNER JOIN
						User um 
						ON 
							t.User_IDmed = um.userID
					INNER JOIN Therapy_List l 
						ON 
							t.TherapyList_IDtherapylist = l.therapy_listID 
					INNER JOIN Medicine m 
						ON 
							l.Medicine_IDmedicine = m.medicineID 
					WHERE t.User_IDpatient = $currentUserID
					ORDER BY
						t.therapyId
				");

        //Used for the form to add patients to a physican
        $patiens = $database->query("SELECT userID, name FROM User WHERE Role_IDrole = 3");
        $therapyList = $database->query("SELECT l.therapy_listID, l.name, l.dosage, m.name as medicineName  FROM Therapy_List l INNER JOIN Medicine m ON l.Medicine_IDmedicine = m.medicineID");

				//Loop over all the therapies and create a 'block' for all of them, containing all information.
        while ($patient = $clients->fetch_assoc()) : ?>
					<div class="section__circle-container mdl-cell mdl-cell--2-col mdl-cell--1-col-phone">
						<div class="section__circle-container__circle mdl-color--primary">
							<img src="<?= $patient['picture']; ?>" class="patient-image" />
						</div>
					</div>
					<div class="section__text mdl-cell mdl-cell--10-col-desktop mdl-cell--6-col-tablet mdl-cell--3-col-phone">
						<h5><?= $patient['name']; ?> (<i><?= $patient['username']; ?>, User ID <?= $patient['userID']; ?></i>)</h5>
						<ul>
							<li>Therapy: <i><?= $patient['therapyName']; ?>  (Id: <?= $patient['therapyID'] ?>)</i></li>
							<li>Medicine: <i><?= $patient['medicineName']; ?> (Dosage: <?= $patient['Dosage']; ?>, ID: <?= $patient['medicineID']; ?>)</i></li>
							<li>Physician: <i><?= $patient['medName']; ?></i></li>
						</ul>
            <?php
            //Get the users tests for the current therapy (joining all tables again)
            $therapyId = $patient['therapyID'];
            $therapy = $database->query("
              SELECT 
                n.noteId, 
                n.note, 
                n.User_IDmed,
                s.type,
                s.DataURL,
                s.test_SessionID,
                t.dateTime,
                t.Therapy_IDtherapy,
                t.testID,
                u.name
              FROM 
                Note n 
              INNER JOIN 
                Test_Session s 
                ON n.Test_Session_IDtest_session = s.test_SessionID
              INNER JOIN 
                Test t 
                ON s.Test_IDtest = t.testID
              INNER JOIN
                User u
                ON n.User_IDmed = u.userID
              WHERE 
                t.Therapy_IDtherapy = $therapyId
            ");

            if($therapy->num_rows === 0){
              //If there is no data available with the last query, let the user know
              echo "No therapy data is yet available for this patient.";
            } else {
              //If there IS data available, loop over it
              while ($therapyRow = $therapy->fetch_assoc()) :
                $randomNumber = mt_rand();
                ?>
								<div class="therapy-info">
									<!-- Allow the user to expand the UL using some dirty inline jQuery. Easy & dirty way to keep it 'tidy' for now. -->
									<h6>Therapy ID: <?= $therapyRow['test_SessionID']?> <span onclick="$('#data-<?= $randomNumber?>').show(300);">(Click here to show)</span></h6>
									<ul id="data-<?= $randomNumber ?>" style="display:none;">
										<li>Date & Time: <?= $therapyRow['dateTime']?></li>
										<li>Data URL: <?= $therapyRow['DataURL']?></li>
										<li>Physican / Researcher: <?= $therapyRow['name']?></li>
										<li>Note: <?= $therapyRow['note']?></li>
									</ul>
								</div>
                <?
              endwhile;
            } //end else (dirty)?>
					</div>
        <? endwhile; ?>
			</div>
		</div>
	</section>
</div>

<script>

  $.ajaxSetup({
    async: false
  	//Turn off async
  });

	//Get the list of users from the api
  $.getJSON("data.php?xml=http://vhost11.lnu.se:20090/final/getData.php?table=User", function(data){
    $.each( data['userID'], function( key, val ) {
			if(val.Role_IDrole == 1){
			  //append all PATIENTS to the list of options
        $('.patient-selector').append($('<option>', {
          //Value is ID of the patient
          value: val['@attributes'].id,
					//Text is usernames and the ID
          text: val.username + " (ID: " + val['@attributes'].id + ")"
        }));
			}
    });
  });

  //If a option for user is chosen
  $('.patient-selector').change(function(){
    $('.therapy-selector').html(""); //Clear the therapy dropdown as new data will be inserted
    if (typeof chart !== 'undefined') {
      //if there is a chart already, unset it because a new one is being chosen
      chart.destroy();
    }

    //If the value is not 0 (which is Choose.. in the dropdown)
    if($(this).val() != 0){
      //Get the selected patients data.
			$.getJSON("data.php?sxml=http://vhost11.lnu.se:20090/final/getFilterData.php?parameter=User_IDpatient%26value=" + $(this).val(), function(data){
				//Add each therapy to the dropdown
			  $.each( data['test_sessionID'], function( key, val ) {
			    //set the data as option, as this is what is to be visualised
					$('.therapy-selector').append($('<option>', {
						value: val.DataURL,
						text: "Therapy trials with Medicine " + val.therapyID + " (ID: " + val.DataURL + ")"
					}));
				});
			});
    }
  });

  $('.therapy-selector').change(function(){
    //If a new therapy is chosen
    if (typeof chart !== 'undefined') {
      //Destroy the current chart
      chart.destroy();
    }

    //Get the data for the chart
    dataIn = getData("http://vhost11.lnu.se:20090/assig2/" + $(this).val() + ".csv", "csv");

    var xData = [{}];
    var yData = [{}];

    //Make new objects from the data for each dataset
    $.each(dataIn, function( key, val ) {
      xData[key] = {"x":val.time, "y":val.x};
      yData[key] = {"x":val.time, "y":val.y};
    });

    //Select the chart element
    var ctx = document.getElementById("canvas");
		//and make a new chart
		chart = new Chart(ctx, {
      type: 'scatter', //scatter chart
      data: {
        datasets: [{ //add the multiple datasets
          label: 'X',
          backgroundColor: "#a8306f", //background color is line color and points
					data: xData //The data set just edited
        },{
          label: 'Y',
          backgroundColor: "#c6e4d3",
          data: yData
        }]
      },
      options: {
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom'
          }]
        }
      }
    });
  });

  //To fix the data with trough the proxy
	function getData(url, datatype){
		var data = $.getJSON("data.php?" + datatype + "=" + url);
		return data['responseJSON'];
	}
</script>
