<?php
/**
 * Created by PhpStorm.
 * User: janvandijk96
 * Date: 09/10/2017
 * Time: 17:39
 */
?>

<section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
	<header class="section__play-btn mdl-cell mdl-cell--3-col-desktop mdl-cell--2-col-tablet mdl-cell--4-col-phone mdl-color--teal-100 mdl-color-text--white">
		<img src="<?= $_SESSION['picture']; ?>" class="profile-picture" />
	</header>
	<div class="mdl-card mdl-cell mdl-cell--9-col-desktop mdl-cell--6-col-tablet mdl-cell--4-col-phone">
		<div class="mdl-card__supporting-text">
			<h4>Welcome, <?= $_SESSION['name']; ?></h4>
			<b>Your personal information (Note: some information might be missing depending on the used service and provided information):</b>

			<ul>
				<li>Birthday: <?= $_SESSION['birthDay'] . '/' . $_SESSION['birthMonth'] . '/' . $_SESSION['birthYear']; ?></li>
				<li>Gender: <?= $_SESSION['gender']; ?></li>
				<li>Email: <?= $_SESSION['email'] ?></li>
				<li>Role: <?= $_SESSION['role'] ?> (
					<?php
						switch ($_SESSION['role']){
							case 1;
								echo "Physician";
								break;
							case 2;
                echo "Researcher";
                break;
							case 3;
								echo "Patient";
								break;
						}	?>
					)</li>
			</ul>

			</div>
		<div class="mdl-card__actions">
			<a href="/authenticate.php?logout" class="mdl-button">Log out</a>
		</div>
	</div>
</section>
