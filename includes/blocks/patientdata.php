<?php
/**
 * Created by PhpStorm.
 * User: janvandijk96
 * Date: 10/10/2017
 * Time: 20:02
 */
?>

<div class="mdl-layout__tab-panel" id="patientdata">
  <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
    <div class="mdl-card mdl-cell mdl-cell--12-col">
      <div class="mdl-card__supporting-text mdl-grid mdl-grid--no-spacing">
        <h4 class="mdl-cell mdl-cell--12-col">Patients Data</h4>
				<select class="patient-selector">
					<option value="0">Choose..</option>
				</select>
				<select class="therapy-selector">

				</select>
				<canvas id="canvas">
					Choose a patient to continue.
				</canvas>
				<div class="comments-list"></div>
				<div class="comment-add" style="display:none;">
					<h3>Add comment</h3>
					<textarea rows="4" cols="50"></textarea>
					<button class="comment-submit">Add comment to data</button>
				</div>

      </div>
    </div>
  </section>
</div>

<script>
	//Turn off async
  $.ajaxSetup({
    async: false
  });

  //Globally define the vars needed in multiple functions
  var lastUserID;
  var lastDataID;

  //get the list of patients
  $.getJSON("data.php?xml=http://vhost11.lnu.se:20090/final/getData.php?table=User", function(data){
    $.each( data['userID'], function( key, val ) {
			if(val.Role_IDrole == 1){
				//only add patients
        $('.patient-selector').append($('<option>', {
          value: val['@attributes'].id,
          text: val.username + " (ID: " + val['@attributes'].id + ")"
        }));
			}
    });
  });

  $('.patient-selector').change(function(){
    $('.therapy-selector').html("");
    if (typeof scatterChart !== 'undefined') {
      scatterChart.destroy();
      $('.comment-add').hide();
    }
    if($(this).val() != 0){

      lastUserID = $(this).val();

			$.getJSON("data.php?sxml=http://vhost11.lnu.se:20090/final/getFilterData.php?parameter=User_IDpatient%26value=" + $(this).val(), function(data){
				$.each( data['test_sessionID'], function( key, val ) {
					$('.therapy-selector').append($('<option>', {
						value: val.DataURL,
						text: "Therapy trials with Medicine " + val.therapyID + " (ID: " + val.DataURL + ")"
					}));
				});
			});
    }
  });

  $('.therapy-selector').change(function(){
    if (typeof scatterChart !== 'undefined') {
      scatterChart.destroy();
      $('.comment-add').hide();
    }

    lastDataID = $(this).val();

    var ctx = document.getElementById("canvas");
		scatterChart = new Chart(ctx, {
      type: 'scatter',
      data: {
        datasets: [{
          label: '<?= $_SESSION['name']; ?>\'s Data',
          data: getData("http://vhost11.lnu.se:20090/assig2/" + $(this).val() + ".csv", "csv")
        }]
      },
      options: {
        scales: {
          xAxes: [{
            type: 'linear',
            position: 'bottom'
          }]
        }
      }
    });

    $(".comments-list").load("comments.php?patient=" + lastUserID + "&data=" + lastDataID + " .comments");
		$('.comment-add').show(300);
  });

  $('.comment-submit').click( function(){
    var comment = $('textarea').val();
    $.get("comments.php",
				{ patient: lastUserID, data: lastDataID, comment: comment, insert: "yes" } )
        .done(function( data ) {
          $(".comments-list").load("comments.php?patient=" + lastUserID + "&data=" + lastDataID + " .comments");
        });
	});

	function getData(url, datatype){
		var data = $.getJSON("data.php?" + datatype + "=" + url);
		return data['responseJSON'];
	}
</script>
