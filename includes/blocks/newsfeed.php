<?php
/**
 * Created by PhpStorm.
 * User: janvandijk96
 * Date: 10/10/2017
 * Time: 17:15
 */

$url = "https://www.news-medical.net/tag/feed/Parkinsons-Disease.aspx"; //Get the XML filme
$xml = simplexml_load_file($url); //Load it.

?>
<div class="mdl-layout__tab-panel" id="newsfeed">
  <section class="section--center mdl-grid mdl-grid--no-spacing mdl-shadow--2dp">
    <div class="mdl-card mdl-cell mdl-cell--12-col">
      <div class="mdl-card__supporting-text">
        <h4>Newsfeed Parkinsons</h4>
        Latest news about Parkinsons in one feed!<br/>
        Source: <?= $xml->channel->title ?>

        <ul class="demo-list-three mdl-list">

          <?php foreach($xml->channel->item as $item) : //Loop over the file and put the needed text in the right places.?>

          <li class="mdl-list__item mdl-list__item--three-line">
            <span class="mdl-list__item-primary-content">
              <i class="material-icons  mdl-list__item-avatar">subject</i>
              <span><?= $item->title ?></span>
              <span class="mdl-list__item-text-body">
                <i><?= $item->pubDate ?></i><br/>
                <?= mb_strimwidth($item->description, 0, 100, "..."); //Trim the description so it doesn't get too loooong.?>
              </span>
            </span>
            <span class="mdl-list__item-secondary-content">
              <a class="mdl-list__item-secondary-action" target="_blank" href="<?= $item->link ?>"><i class="material-icons">link</i></a>
            </span>
          </li>

          <? endforeach; ?>

        </ul>
      </div>
      <div class="mdl-card__actions">
        <a href="<?= $url //Use the url :) ?>" class="mdl-button">Source</a>
      </div>
    </div>
  </section>
</div>