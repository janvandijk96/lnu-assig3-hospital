<?php
/**
 * Created by PhpStorm.
 * User: janvandijk96
 * Date: 10/10/2017
 * Time: 20:24
 */

if(is_localhost()){ //Check if running on localhost. Use settings accordingly.
  error_reporting(E_ALL);
  $callbackURL = 'http://janvandijk.local/authenticate.php';
  $database = mysqli_connect("janvandijk.me", "hidden", "hidden", "hidden");
} else {
  error_reporting(0);
  $callbackURL = 'http://lnu-assig3.janvandijk.me/authenticate.php';
  $database = mysqli_connect("localhost", "hidden", "hidden", "hidden");
}


if (!$database) { //Generic way to display error messages.
  echo "Error: Unable to connect to MySQL." . PHP_EOL;
  echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
  echo "Debugging error: " . mysqli_connect_error() . PHP_EOL;
  die();
}

//Found on https://stackoverflow.com/questions/2053245/how-can-i-detect-if-the-user-is-on-localhost-in-php
function is_localhost() {
  $whitelist = array( '127.0.0.1', '::1' );
  if( in_array( $_SERVER['REMOTE_ADDR'], $whitelist) )
    return true;
}

$googleMapsApiKey = "AIzaSyDwHQplajPJn-enFFNJO92xxsNhsxxp8oQ";
$youtubeApiKey = "AIzaSyCfUWYzewNgA6lPnHjUUdvE2obxZkaFmW4";