<?php

/**
 * Created by PhpStorm.
 * User: janvandijk96
 * Date: 09/10/2017
 * Time: 17:36
 */
?>
<header class="mdl-layout__header mdl-layout__header--scroll mdl-color--primary">
  <div class="mdl-layout--large-screen-only mdl-layout__header-row">
  </div>
  <div class="mdl-layout--large-screen-only mdl-layout__header-row">
    <h3>Parkinsons Hospital</h3>
  </div>
  <div class="mdl-layout--large-screen-only mdl-layout__header-row">
  </div>
  <div class="mdl-layout__tab-bar mdl-js-ripple-effect mdl-color--primary-dark">
    <a href="#overview" class="mdl-layout__tab is-active"><? if(isset($_SESSION['name'])){ echo "Overview"; } else { echo "Log In.."; }?></a>
		<? if(isset($_SESSION['role'])) :
						switch ($_SESSION['role']){ //Based on role, display the tabs for the blocks which are included (see explaination in index.php)
              case 1;?>
								<a href="#patients" class="mdl-layout__tab">Patients</a>
								<a href="#patientdata" class="mdl-layout__tab">Patients data</a>
                <? break;
              case 2; ?>
								<a href="#patients" class="mdl-layout__tab">Patients</a>
								<a href="#newsfeed" class="mdl-layout__tab">Newsfeed</a>
								<a href="#patientdata" class="mdl-layout__tab">Patient Data</a>
								<a href="#maps" class="mdl-layout__tab" onclick="initMap()">Maps</a>
								<? break;
              case 3;?>
								<a href="#yourdata" class="mdl-layout__tab">Your Data</a>
								<a href="#videos" class="mdl-layout__tab">Videos</a>
                <? break;
            }	?>
		<? endif; ?>


		<? if(isset($_SESSION['name'])) : //Logout, but only show if logged in. ?>
    <a href="/authenticate.php?logout" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored mdl-shadow--4dp mdl-color--accent" id="add">
			<i class="material-icons" role="presentation">exit_to_app</i>
    </a>
		<? endif; ?>

  </div>
</header>
