<?php

session_start();

require "vendor/autoload.php";
require "includes/settings.php";

//First step is to build a configuration array to pass to `Hybridauth\Hybridauth`
$config = [
  //Location where to redirect users once they authenticate with a provider
  'callback' => $callbackURL, //Set in includes/settings.php

  //Providers specifics
  'providers' => [
      'Facebook' => [
          'enabled' => true,
          'keys' => [
              'id'  => '135529270508351',
              'secret' => '6277a9905244a8a1997a150b5fa5968f'
          ]
      ],

      'Twitter' => [
        'enabled' => true,
        'keys' => [
          'key'  => 'jKAC3UR1TLC2NB1jVTz2qBv1h',
          'secret' => 'BROoQNX7CKgLB7VKYrLDG5YZbfVHJxcnqEpdAj5otqGoG2ujLD'
          ]
      ],

      'LinkedIn' => [
        'enabled' => true,
          'keys' => [
            'id'  => '775eb6c1cz1a7h',
            'secret' => 'eEZ5nMH5xbsQ0TFa'
          ]
      ]
  ]
];

if(isset($_GET['logout'])){
  //Log the user out by destroying the session. The connection by Hybridauth is already closed if this route is requested!
  session_destroy();
  $_SESSION['loggedIn'] = false;
  header("location: /");
  die(); //Die just to make sure the script does not continue.
}

if(isset($_GET['p'])){
  //If P is set, this means a new authentication is requested. To be sure, destroy the current session (altough requesting the login route should not be possible.)
  //session_destroy();
  //Set the last auth service for when the user returns from the service, used later in the code.
  $_SESSION['lastAuthService'] = $_GET['p'];
}

try{
  //Feed configuration array to Hybridauth
  $hybridauth = new Hybridauth\Hybridauth($config);


  //Attempt to authenticate users with a provider by name. The requested service is remembered in the session!
  $adapter = $hybridauth->authenticate($_SESSION['lastAuthService']);

  //Returns a boolean of whether the user is connected with Service
  $isConnected = $adapter->isConnected();

  //Retrieve the user's profile
  $userProfile = $adapter->getUserProfile();


  if(isset($userProfile)){
    //Set session info based on response
    $_SESSION['loggedIn'] = true;
    $_SESSION['identifier'] = $userProfile->identifier;
    $_SESSION['name'] = $userProfile->displayName;
    $_SESSION['birthMonth'] = $userProfile->birthMonth;
    $_SESSION['birthDay'] = $userProfile->birthDay;
    $_SESSION['birthYear'] = $userProfile->birthYear;
    $_SESSION['gender'] = $userProfile->gender;
    $_SESSION['picture'] = $userProfile->photoURL;
    $_SESSION['email'] = $userProfile->email;

    //Set role according to platform
    if($_SESSION['lastAuthService'] == "Facebook"){
      $_SESSION['role'] = 1; //Physician role
    } else if ($_SESSION['lastAuthService'] == "Twitter"){
      $_SESSION['role'] = 2; //researcher role
    } else if ($_SESSION['lastAuthService'] == "LinkedIn"){
      $_SESSION['role'] = 3; //patient role
    }

    //Now query to look if the user is already registered or not
    //Use tidy vars for queries
    $username = $_SESSION['identifier'];
    $name = $_SESSION['name'];
    $email = $_SESSION['email'];
    $picture = $_SESSION['picture'];
    $Role_IDrole = $_SESSION['role'];
    $organisation = 3;

    //Query to check if user exists
    $result = $database->query("SELECT * FROM User WHERE username = '" . $_SESSION['identifier'] . "'");

    //Check if the user exists
    if($result->num_rows === 0){
      //User does not exist, create
      $result = $database->query("INSERT INTO User VALUES (null, '$username', '$name', '$email', '$picture', $Role_IDrole, $organisation)");
    } else {
      //User exists, update info
      $result = $database->query("UPDATE User SET email = '$email', picture = '$picture', Organization = $organisation WHERE username = '" . $_SESSION['identifier'] . "'");
    }
    //Request the user ID
    $result = $database->query("SELECT * FROM User WHERE username = '" . $_SESSION['identifier'] . "'")->fetch_all();
    $_SESSION['userId'] = $result[0][0]; //User ID is in [0][0]
  }

  //Disconnect the adapter
  $adapter->disconnect();

  header("location: /"); //Return the user to the app!
}
catch(\Exception $e){
  echo 'FOUTJE BEDANKT, we ran into an issue! ' . $e->getMessage(); //Something went wrong.
}